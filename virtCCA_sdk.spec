Name:			virtCCA_sdk
Version:		0.1.3
Release:		1
ExclusiveArch:	aarch64
Summary:		%{name} is a SDK for developing virtCCA remote attestation and key derivation.

License:		MulanPSL-2.0
URL:			https://gitee.com/openeuler/virtCCA_sdk
Source0:		https://gitee.com/openeuler/virtCCA_sdk/repository/archive/virtCCA_sdk-v%{version}.tar.gz

BuildRequires:	gcc glibc glibc-devel glib2-devel cmake openssl openssl-devel rpm

Requires:	openssl-libs

%description
%{name} is a SDK for developing virtCCA remote attestation and key derivation.

%package	devel
Summary:	Development files for %{name}
Requires:	virtCCA_sdk openssl-libs

%description	devel
%{name}-devel is a SDK for developing virtCCA remote attestation and key derivation.

%prep
%autosetup -n %{name}-v%{version}

%build
cd attestation/rim_ref
cmake -S . -B build
cmake --build build

cd ../sdk
cmake -S . -B build
cmake --build build

cd ../../sealing_key/sdk
cmake -S . -B build
cmake --build build

%install
install -d %{buildroot}/%{_bindir}
install -pm 755 attestation/rim_ref/output/gen_rim_ref %{buildroot}/%{_bindir}

install -d %{buildroot}/%{_libdir}
install -pm 644 attestation/sdk/build/libvccaattestation.a %{buildroot}/%{_libdir}
install -pm 644 sealing_key/sdk/build/libsealingkey.so %{buildroot}/%{_libdir}

install -d %{buildroot}/%{_includedir}
install -pm 644 attestation/sdk/include/attestation.h %{buildroot}/%{_includedir}
install -pm 644 sealing_key/sdk/inc/sealing_key.h %{buildroot}/%{_includedir}

%files
%license License/LICENSE
%license License/Third_Party_Open_Source_Software_Notice.md
%defattr(-,root,root)
%{_bindir}/gen_rim_ref
%{_libdir}/libsealingkey.so

%files	devel
%defattr(-,root,root)
%{_includedir}/attestation.h
%{_includedir}/sealing_key.h
%{_libdir}/libvccaattestation.a

%changelog
* Sat Nov 30 2024 chenzheng<chenzheng71@huawei.com> - 0.1.3-1
- DESC:chage sealing key interface parameter for avoid ambiguity

* Wed Jun 26 2024 lishengjie<lishengjie12@huawei.com> - 0.1.2-1
- DESC:Merge read token interface

* Fri May 31 2024 lishengjie<lishengjie12@huawei.com> - 0.1.1-1
- DESC:Add sealing key lib

* Tue May 28 2024 lishengjie<lishengjie12@huawei.com> - 0.1.0-1
- DESC:init virtCCA_sdk
